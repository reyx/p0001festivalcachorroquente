<?
class Apoiador extends AppModel {
    public $name = 'Apoiador';

    public $displayField = 'empresa';

    /*public $validate = array(
        'email' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'telefone' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'responsavel' => array(
            'rule' => 'notEmpty',
            'required' => true
        )
    );*/

    public $displayFieldTypes = array(
        'logo' => 'image',
        'logo' => 'file',
    );

    public $upLoads = array(
        'imgDir' => 'apoiadores',
        'itemDir' => array('field' => 'logo'),
    );

    public $ignoreFieldList = array(
        'logo_dir',
        'logo',
    );

    public $actsAs = array(
        'Upload.Upload' => array(
            'logo' => array(
                'fields' => array(
                    'dir' => 'logo_dir'
                )
            )
        )
    );

}
