<?
    App::uses('BrValidation', 'Localized.Validation');

class Dogueiro extends AppModel {
    public $name = 'Dogueiro';
    public $hasMany = array('Classificacao');
    public $displayField = 'nome';

    public $_schema = array(
        'numero' => array('type' => 'string', 'length' => 11),
        'nome' => array('type' => 'string', 'length' => 155),
        'email' => array('type' => 'string', 'length' => 155),
        'foto' => array('type' => 'string', 'length' => 255, 'null' => true),
        'foto_dir' => array('type' => 'string', 'length' => 255, 'null' => true),
        'facebook_foto' => array('type' => 'string', 'length' => 255, 'null' => true),
        'facebook_foto_dir' => array('type' => 'string', 'length' => 255, 'null' => true),
        'cpf' => array('type' => 'string', 'length' => 11),
        'telefone' => array('type' => 'string', 'length' => 20)
    );

    public $validate = array(
        'email' => array(
            'rule' => 'email',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Email inválido.'
        ),
        'nome' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'É necessário preencher o nome.'
        ),
        'telefone' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'É necessário preencher o telefone.'
        ),
        'cpf' => array(
            'rule' => array('ssn', null, 'br'),
            'message' => 'CPF inválido',
            'allowEmpty' => false,
            'required' => true
        )
    );

    var $displayFieldTypes = array(
        'foto' => 'image',
        'foto' => 'file',
        'facebook_foto' => 'image',
        'facebook_foto' => 'file',
    );

    var $upLoads = array(
        'imgDir' => 'dogueiros',
        'itemDir' => array('field' => 'foto'),
    );

    var $ignoreFieldList = array(
        'foto_dir',
        'foto',
        'facebook_foto_dir',
        'facebook_foto',
    );

    public $actsAs = array(
        'Upload.Upload' => array(
            'foto' => array(
                'fields' => array(
                    'dir' => 'foto_dir'
                ),
                # 'pathMethod' => 'flat'
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '[130x77]'
                )
            ),
            'facebook_foto' => array(
                'fields' => array(
                    'dir' => 'facebook_foto_dir'
                ),
                # 'pathMethod' => 'flat'
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '[560x420]'
                )
            )
        )
    );
}
