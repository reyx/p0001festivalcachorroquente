<?
class Foto extends AppModel {
    public $name = 'Foto';
    public $belongsTo = 'Makingof';

    // public $belongsTo = array('Makingof');

    var $adminSettings = array(
        'icon' => 'blog',
    );

    var $displayFieldTypes = array(
        'img' => 'file',
    );

    var $upLoads = array(
        'imgDir' => 'imagens',
        'itemDir' => array('field' => 'img'),
    );

    var $ignoreFieldList = array(
        'img_dir',
        'img',
    );

    public $actsAs = array(
        'Upload.Upload' => array(
            'img' => array(
                'fields' => array(
                    'dir' => 'img_dir'
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '[130x77]'
                )
            )
        )
    );

    public $validate = array(
        'img' => array(
            'rule' => 'isUnderPhpSizeLimit',
            'message' => 'File exceeds upload filesize limit'
        ),
        'img' => array(
            'rule' => 'isUnderFormSizeLimit',
            'message' => 'File exceeds form upload filesize limit'
        ),
        'img' => array(
            'rule' => 'tempDirExists',
            'message' => 'The system temporary directory is missing'
        ),
        'img' => array(
            'rule' => 'isSuccessfulWrite',
            'message' => 'File was unsuccessfully written to the server'
        ),
        'img' => array(
            'rule' => array('isWritable'),
            'message' => 'File upload directory was not writable'
        ),
        'img' => array(
            'rule' => array('isValidDir'),
            'message' => 'File upload directory does not exist'
        ),
        'img' => array(
            'rule' => array('isBelowMaxSize', 101024),
            'message' => 'File is larger than the maximum filesize'
        )
    );

}
