<div class="form-signin">
    <? echo $this->Html->link(
        $this->Html->image("web/logo.png", array("alt" => "2º Festival do Cachorro Quente de Osasco - SP", "title" => "2º Festival do Cachorro Quente de Osasco - SP", "id" => "lateral_logo")),
        "/",
        array('escape' => false)
    ) ?>

    <div><?php
        echo $this->Session->flash();
        echo $this->Form->create('User', array( 'controller' => 'Users', 'action' => 'login' ), array('class' => 'form-horizontal') );
        echo $this->Form->input('username', array('label' => 'Usuário', 'class' => 'span12'));
        echo $this->Form->input('password', array('label' => 'Senha', 'class' => 'span12'));
        echo $this->Form->submit('Login', array('class' => 'btn btn-primary'));
        echo $this->Form->end();
    ?></div>
</div>