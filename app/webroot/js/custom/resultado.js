$(function() {
    var total = 0;
    $('#resultado tbody tr td:last-child').each(function(i, row) {
        total += parseInt($(row).text())
    });
    $('#total').text(total);
});