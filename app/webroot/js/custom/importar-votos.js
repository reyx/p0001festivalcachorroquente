var $alerta = $('#alerta');
var $votos;
$(function() {
    $('input[type="file"]').on('change', function(e) {
        var files = e.target.files; // FileList object

        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onloadend = function(e) {
                $votos = [];
                try {
                    if (e.target.readyState === FileReader.DONE) {
                        $votos = $($votos.concat($.parseJSON(e.target.result))).map(function(i, row) {
                            delete row.Voto.id;
                            return row.Voto;
                        });
                        $('#arquivos').text(['[', $votos.length, ']', 'votos válidos'].join(' '));
                        $('#validos').text($votos.length);
                    }
                }
                catch(ex) {
                    $('#arquivos').text('Houve uma falha ao carregar os arquivos: ' + ex);
                }
            };
            reader.readAsText(f);
        }
    });
});
var importados = 0;
var submit = function() {
    if ($votos.length === 0) return;
    $alerta.addClass("alert").text('Processando votos...');
    send(true);
};

var send = function(clear) {
    var chunk = $votos.splice(0, 50);
    $.post(window.location.pathname + '.json', { clear: clear, votos: JSON.stringify(chunk) })
        .done(function(data) {
            if (data.result) {
                importados += data.total;
                $('#validos').text($votos.length);
                if ($votos.length > 0) {
                    send(false);
                } else {
                    $alerta.removeClass('alert-error').addClass('alert-info').text(['[', importados, ']', 'votos importados com sucesso!'].join(' '));
                }
            } else {
                $alerta.removeClass('alert-info').addClass('alert-error').text(data.error);
            }
        })
        .fail(function(data) {
            $alerta.removeClass('alert-error').addClass('alert-info').text(data.message);
        });
};