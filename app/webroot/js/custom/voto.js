$(function() {
    $('.key').on('click', function(){
        var numero = $('#VotoNumero').val();
        if (numero.length < 3) {
            $('#VotoNumero').val(numero + $(this).val()).trigger('change');
        }
    });

    $('button[type="reset"]').on('click', function(){
        $('#dogueiro-foto').hide().attr('src', '');
        $('#dogueiro-nome').text('');
        $('#VotoNumero').val('');
        $('#VotoDogueiroId').val('');
    });

    $('button[type="reset"]').on('click', function(){
        var numero = $('#VotoNumero').val();
        $('#dogueiro-foto').fadeOut();
        $('#dogueiro-nome').text('');
        $('#VotoNumero').val('');
        $('#VotoDogueiroId').val('');
    });

    var _to_ascii = {
        '188': '44',
        '109': '45',
        '190': '46',
        '191': '47',
        '192': '96',
        '220': '92',
        '222': '39',
        '221': '93',
        '219': '91',
        '173': '45',
        '187': '61', //IE Key codes
        '186': '59', //IE Key codes
        '189': '45'  //IE Key codes
    }

    $('#VotoNumero').on('change keydown', function(e) {
        if (e.type === 'keydown') {
            // Allow: backspace, delete, tab, escape, and enter
            var key = (e.keyCode ? e.keyCode : e.which), c;

            //normalize keyCode
            if (_to_ascii.hasOwnProperty(key)) {
                key = _to_ascii[key];
            }

            c = String.fromCharCode(key - (key >= 96 && key <= 105 ? 48 : 0));

            if ( key == 46 || key == 8 || key == 9 || key == 27 || key == 13 ||
                // Allow: Ctrl+A
                (key == 65 && event.ctrlKey === true) ||
                // Allow: home, end, left, right
                (key >= 35 && key <= 39)) {
                // let it happen, don't do anything
                return;
            } else {
                // Ensure that it is a number and stop the keypress
                if (e.shiftKey || (key < 48 || key > 57) && (key < 96 || key > 105 )) {
                    e.preventDefault();
                    return;
                }
            }

            if ($('#VotoNumero').val().length < 3) {
                $('#VotoNumero').val($('#VotoNumero').val() + c);
            } else {
                return;
            }

            e.preventDefault();
        }

        if ($('#VotoNumero').val().length === 3) {
            $('#dogueiro-nome').removeClass('text-error').text('Localizando dogueiro...');
            $.get(config.root + 'dogueiros/dogueiro/' + $('#VotoNumero').val(), null, function(data){
                if (data.length === 0) {
                    $('#dogueiro-foto').hide().attr('src', '');
                    $('#dogueiro-nome').addClass('text-error').text('Dogueiro não localizado.');
                    $('#VotoDogueiroId').val('');
                } else {
                    $('#dogueiro-foto').attr('src', config.root + ['files', 'dogueiro', 'foto', data.Dogueiro.foto_dir, data.Dogueiro.foto].join('/')).fadeIn();
                    $('#dogueiro-nome').text(data.Dogueiro.nome);
                    $('#VotoDogueiroId').val(data.Dogueiro.id);
                }
            });
        }
    });
});