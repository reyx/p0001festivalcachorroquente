$(function() {
    $(".fancybox img").imagesLoaded(function(){
        $(this).parent().parent().addClass('in');
    });
    $(".fancybox").fancybox({
        "openEffect": "elastic",
        "closeEffect": "elastic",
        "nextEffect": "fade",
        "prevEffect": "fade",
        "scrolling": "no",
        "helpers": {
            "title": { type : "inside" },
            "buttons": {}
        }
    });
});