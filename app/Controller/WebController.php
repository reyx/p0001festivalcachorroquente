<?
class WebController extends AppController {
    var $name = 'Web';

    public function index() {
        $this->loadModel('Materia');

        $destaques = $this->Materia->find('all',
            array(
                'conditions' => array('Materia.type <' => 2),
                'order' => array('position')
            )
        );
        $chamadas = $this->Materia->find('all', array('conditions' => array('Materia.type =' => 2)));

        $this->set('destaques', $destaques);
        $this->set('chamadas', $chamadas);
    }
}
