<?
    class SobreController extends AppController {

        public function index() {

        }

        public function introducao() {

        }

        public function concurso() {

        }

        public function programacao() {
            $this->loadModel('Atividade');

            $atividades = $this->Atividade->find('all',
                array(
                    'order' => array('Atividade.hora')
                )
            );

            $this->set('atividades', $atividades);
        }

        public function premiacoes() {

        }

        public function localizacao() {

        }

        public function equipe() {

        }

    }
?>
