<?php
/**
 *@property Voto $Voto
 */
class VotosController extends AppController {

    public $components = array('Auth', 'RequestHandler');

    function beforeFilter() {
        $this->Auth->loginAction = '/admin/users/login';
        $this->Auth->loginRedirect = array('controller' => 'votos', 'action' => 'resultado');
        $this->Auth->userModel = 'User';
        $this->Auth->allow('index', 'obrigado', 'importar');
        $this->Auth->deny('resultado', 'exportar');

        $user = $this->Session->read('User');

        if ($user) {
            $this->Auth->login($user);
        }
    }

    public function resultado() {
        $votos = $this->Voto->find('all', array(
            'joins' => array(
                array(
                    'table' => 'dogueiros',
                    'alias' => 'Dogueiro',
                    'conditions' => array('Dogueiro.id = Voto.dogueiro_id')
                )
            ),
            'fields' => array
            (
                'numero' => 'Dogueiro.numero',
                'nome' => 'Dogueiro.nome',
                'votos' => 'COUNT(*) AS votos'
            ),
            'group' => array (
                'Dogueiro.numero',
                'Dogueiro.nome'
            ),
            'order' => array (
                'COUNT(*) DESC'
            )
        ));
        $this->set('votos', $votos);
    }

    public function exportar() {
        $votos = $this->Voto->find('all');
        $this->set(array(
            'votos' => $votos,
            '_serialize' => 'votos'
        ));
    }

    public function importar() {
        if ($this->request->is('ajax')) {
            try {
                $clear = $this->request->data['clear'] == "true";
                $votos = json_decode($this->request->data['votos']);
                $total = count($votos);
                $result = true;
                if ($clear) {
                    $result = $this->Voto->deleteAll(array('Voto.id > 0'));
                }

                if ($result) {
                    $this->Voto->saveMany($votos, array('validate' => false));
                }

                $this->set(array(
                    'result' => true,
                    'total' => $total,
                    '_serialize' => array('result', 'total')
                ));
            }
            catch (Exception $ex) {
                $this->set(array(
                    'result' => false,
                    'message' => $ex->getTraceAsString(),
                    '_serialize' => array('result', 'message')
                ));
            }
        }
    }

    public function obrigado() {
    }

    public function index() {
        try {
            $this->layout = 'default';
            if ($this->request->is('post')) {
                $this->loadModel('Dogueiro');
                $count = $this->Dogueiro->find('count', array(
                    'conditions' => array(
                        'numero =' => $this->request->data['Voto']['numero']
                    )
                ));
                if($count == 0) {
                    $this->Session->setFlash('Dogueiro não localizado.', 'flash_error');
                }
                /*$count = $this->Voto->find('count', array(
                    'conditions' => array(
                        ' Voto.ip = ' => $this->request->clientIp(),
                        ' DATE(Voto.data) = ' => date('Y-m-d')
                    )
                ));
                if($count > 0) {
                    $this->Session->setFlash('O seu voto já foi computado hoje.', 'flash_error');
                }
                else {*/
                    $dogueiro_id = $this->request->data['Voto']['dogueiro_id'];

                    if ($this->Dogueiro->find('count', array('conditions' => array("id" => $dogueiro_id))) == 0) {
                        $this->Session->setFlash('Dogueiro não localizado.', 'flash_error');
                    }
                    else {
                        $this->Voto->create();
                        if ($this->request->is('post') || $this->request->is('put')) {
                            $this->Voto->set('ip', $this->request->clientIp());
                            $this->Voto->set('data', date('Y-m-d H:i:s'));
                            if ($this->Voto->save($this->request->data)) {
                                $this->redirect(array('action' => 'obrigado', $this->Voto->id));
                            } else {
                                $this->Session->setFlash('Não foi possível registrar o voto. Tente novamente.');
                            }
                        }
                    }
                //}
            }
        }
        catch(Exception $ex) {
            $this->Session->setFlash($ex->getMessage());
        }
    }

}