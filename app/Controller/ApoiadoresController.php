<?php
    class ApoiadoresController extends AppController {
        public $helpers = array('Html', 'Form', 'Session');
        public $components = array('Session');

        public function index() {
            $apoiadores = $this->Apoiador->find('all',
                array(
                    'conditions' => array('Apoiador.apoiador' => 1),
                    'order' => array('posicao')
                )
            );
            $patrocinadores = $this->Apoiador->find('all',
                array(
                    'conditions' => array('Apoiador.patrocinador' => 1),
                    'order' => array('posicao')
                )
            );

            $this->set('apoiadores', $apoiadores);
            $this->set('patrocinadores', $patrocinadores);
        }

        public function cadastro() {
            if ($this->request->is('post') || $this->request->is('put')) {
                $conditions = array(
                    'conditions' => array(
                        "OR" => array(
                            "email" => $this->request->data['Apoiador']["email"]
                        )
                    )
                );

                if ($this->Apoiador->find('count', $conditions) > 0) {
                    $this->Session->setFlash('Apoiador já cadastrado com este e-mail', 'flash_error');
                }
                else {
                    $this->Apoiador->set($this->request->data);
                    if ($this->Apoiador->validates()) {
                        if ($this->Apoiador->save()) {
                            $this->redirect(array('action' => 'obrigado', $this->Apoiador->id));
                        } else {
                            $this->Session->setFlash('Não foi possível concluir o cadastro. Tente novamente.', 'flash_error');
                        }
                    }
                }
            }
        }

        public function obrigado() {

        }

    }
