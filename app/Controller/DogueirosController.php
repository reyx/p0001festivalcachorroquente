<?
/**
 *@property Dogueiro $Dogueiro
 */
class DogueirosController extends AppController {

    public function obrigado($id) {
        $dogueiro = $this->Dogueiro->find('first', array(
            'conditions' => array('id' => $id)
        ));
        $this->set('dogueiro', $dogueiro);
    }

    public function index() {
        try {
            $this->set('numero', 500 + $this->Dogueiro->find('count'));

            if ($this->request->is('post') || $this->request->is('put')) {

                $cpf = $this->request->data['Dogueiro']['cpf'];
                $cpf = preg_replace("/[^a-zA-Z0-9]+/", "", $cpf);

                $this->request->data['Dogueiro']['cpf'] = $cpf;

                $conditions = array(
                    'conditions' => array(
                        "OR" => array(
                            "cpf" => $cpf,
                            "email" => $this->request->data['Dogueiro']["email"]
                        )
                    )
                );

                if ($this->Dogueiro->find('count', $conditions) > 0) {
                    $this->Session->setFlash('Dogueiro já cadastrado com este de número de CPF e/ou e-mail', 'flash_error');
                }
                else {
                    $this->Dogueiro->set($this->request->data);
                    if ($this->Dogueiro->validates()) {
                        if ($this->Dogueiro->save()) {
                            $this->redirect(array('action' => 'obrigado', $this->Dogueiro->id));
                        } else {
                            $this->Session->setFlash('Não foi possível concluir o cadastro. Tente novamente.', 'flash_error');
                        }
                    }
                }
            }
        }
        catch(Exception $ex) {
            $this->Session->setFlash($ex->getMessage());
        }
    }

    public function galeria() {
        $dogueiros = $this->Dogueiro->find('all', array (
            'order' => 'Dogueiro.numero'
        ));
        $this->set('dogueiros', $dogueiros);
    }

    public function top30($ano) {
        try {
            $this->loadModel('Classificacao');
            $list = $this->Classificacao->find('all',
                array(
                    'conditions' => array('Classificacao.ano = ' => $ano),
                    'order' => array('Dogueiro.nome', 'Classificacao.posicao'),
                    'limit' => 30
                )
            );
            $this->set('list', $list);
        }
        catch(Exception $ex) {
            $this->Session->setFlash($ex->getMessage());
            $this->set('list', null);
        }
    }

    public function dogueiro($numero) {
        // $this->set('_serialize', array('dogueiro'));

        $dogueiro = $this->Dogueiro->find('first', array(
            'conditions' => array( "and" => array(
                "Dogueiro.numero = " => $numero,
                "Dogueiro.numero < " => 1000
            )),
            'fields' => array(
                'Dogueiro.id',
                'Dogueiro.nome',
                'Dogueiro.numero',
                'Dogueiro.email',
                'Dogueiro.foto_dir',
                'Dogueiro.foto',
            )
        ));

        return new CakeResponse(array('body' => json_encode($dogueiro), 'type' => 'json'));
    }

}
