<? $this->set("title_for_layout", "Os bastidores"); ?>

<?
    $this->Html->css('fancybox/jquery.fancybox.css', null, array('inline' => false));
    $this->Html->css('fancybox/jquery.fancybox-buttons.css', null, array('inline' => false));

    $this->Html->script('fancybox/jquery.mousewheel-3.0.6.pack.js', array('inline' => false));
    $this->Html->script('fancybox/jquery.fancybox.pack.js', array('inline' => false));
    $this->Html->script('fancybox/jquery.fancybox-buttons.js', array('inline' => false));
    $this->Html->script('jquery.imagesloaded.min.js', array('inline' => false));
    $this->Html->script('custom/bastidores.js', array('inline' => false));
?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao/">Sobre o Festival</a> <span class="divider">&raquo;</span></li>
    <li>Os Bastidores / Making-Ofs 2012</li>
</ul>

<? foreach ($makingofs as $makingof_index => $makingof): ?>
    <? if ($makingof['Foto']): ?>
    <div class="row-fluid makingof fade">
        <h4><? echo $makingof['Makingof']['title'] ?> <span class="label"><? echo $makingof['Makingof']['label'] ?></span></h4>

        <? foreach ($makingof['Foto'] as $image_index => $image): ?>
            <?php echo $this->Html->link(
                $this->Html->image('/files/foto/img/' . $image['img_dir'] . '/thumb_' . $image['img'], array('alt' => '')),
                '/files/foto/img/' . $image['img_dir'] . '/' . $image['img'],
                array(
                    'class' => 'fancybox',
                    'rel' => 'gallery' . $makingof['Makingof']['id'],
                    'title' => $makingof['Makingof']['title'],
                    'escape' => false
                )
            ) ?>
        <? endforeach; ?>
    </div>
    <? endif; ?>
<? endforeach; ?>

<p class="more"><? echo $this->Html->link('Saiba mais...', array('controller' => 'sobre', 'action' => 'concurso')) ?></p>