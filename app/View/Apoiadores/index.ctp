<? $this->assign('sidebar', ''); ?>

<? $this->set("title_for_layout", "Nossos apoiadores"); ?>

<ul class="breadcrumb">
    <li>Nossos apoiadores</li>
</ul>

<p>
    <?php echo $this->Html->link('Quero ser um apoiador / patrocinador', array('controller' => 'apoiadores', 'action' => 'cadastro'), array('target' => '_top')) ?>
</p>

<? foreach ($apoiadores as $index => $apoiador): ?>
    <? if ($index == 0): ?>
    <div class="row-fluid">
    <? endif ?>
        <div class="span6 text-center">
            <?php if ($apoiador['Apoiador']['link'] == '') {
                echo $this->Html->image(
                    '/files/apoiador/logo/' . $apoiador['Apoiador']['logo_dir'] . '/' . $apoiador['Apoiador']['logo'],
                    array('title' => $apoiador['Apoiador']['label'])
                );
            } else {
                echo $this->Html->link(
                    $this->Html->image(
                        '/files/apoiador/logo/' . $apoiador['Apoiador']['logo_dir'] . '/' . $apoiador['Apoiador']['logo'],
                        array('alt' => $apoiador['Apoiador']['label'])
                    ),
                    $apoiador['Apoiador']['link'],
                    array('target' => '_blank', 'title' => $apoiador['Apoiador']['label'], 'escape' => false)
                );
            } ?>
        </div>
    <? if ($index & 1 == 1): ?>
    </div>
    <div class="row-fluid">
    <? endif ?>
    <? if ($index == (sizeof($apoiadores) - 1)): ?>
    </div>
    <? endif ?>
<? endforeach; ?>

<ul class="breadcrumb">
    <li>Nossos patrocinadores</li>
</ul>
<? foreach ($patrocinadores as $index => $apoiador): ?>
    <? if ($index == 0): ?>
    <div class="row-fluid">
    <? endif ?>
        <div class="span6 text-center patrocinador">
            <?php if ($apoiador['Apoiador']['link'] == '') {
                echo $this->Html->image(
                    '/files/apoiador/logo/' . $apoiador['Apoiador']['logo_dir'] . '/' . $apoiador['Apoiador']['logo'],
                    array('title' => $apoiador['Apoiador']['label'])
                );
            } else {
                echo $this->Html->link(
                    $this->Html->image(
                        '/files/apoiador/logo/' . $apoiador['Apoiador']['logo_dir'] . '/' . $apoiador['Apoiador']['logo'],
                        array('alt' => $apoiador['Apoiador']['label'])
                    ),
                    $apoiador['Apoiador']['link'],
                    array('target' => '_blank', 'title' => $apoiador['Apoiador']['label'], 'escape' => false)
                );
            } ?>
        </div>
    <? if ($index & 1 == 1): ?>
    </div>
    <div class="row-fluid">
    <? endif ?>
    <? if ($index == sizeof($patrocinadores) - 1): ?>
    </div>
    <? endif ?>
<? endforeach; ?>
