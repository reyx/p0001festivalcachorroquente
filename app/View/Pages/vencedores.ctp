<? $this->set("title_for_layout", "Vencedores 2012"); ?>

<?php $this->set('hideRelease', true); ?>

<ul class="breadcrumb">
    <li>Os vencedores de 2012</li>
</ul>

<p>A organização parabeniza e agradece a participação de todos os dogueiros.<br />
Desejamos um grande sucesso à todos vocês, vencedores de todos os dias.<br />
Todos vocês merecem esta homenagem.</p><br />

<img src="<? echo $this->webroot . 'img/web/vencedores.jpg' ?>" alt="Os 3 vencedores" title="Os 3 vencedores" />

<br>
<br>

<p>1º Colocado:</span> <strong><u>Luciana Fabiano (Rock Dog)</u></strong></p>
<blockquote>Av. Dr. Carlos de Moraes Barros, 573 - Vila Campesina - Osasco/SP</blockquote>

<p>2º Colocado:</span> <strong><u>Joyce Pereira Pinheiro de Oliveira (Família Hot Dog)</u></strong></p>
<blockquote>Calçadão da Antônio Agú - Centro - Osasco/SP</blockquote>

<p>3º Colocado:</span> <strong><u>Gisele Daiani Rala</u></strong></p>
<blockquote>Calçadão da Antônio Agú - Centro - Osasco/SP</blockquote>

<p><?php echo $this->Html->link('Confira a lista com os <b>30 melhores</b> de Osasco...', array('controller' => 'dogueiros', 'action' => 'top30', date('Y', strtotime("-1 year"))), array('escape' => false)); ?></p>

<ul class="breadcrumb">
    <li>O maior comedor de cachorro-quente de Osasco</li>
</ul>

<p>Campeão: <strong><u>Victor Hugo Casagrande</u></strong></p>
