<? $this->set("title_for_layout", "O Município de Osasco"); ?>

<?php $this->set('hideBanner', true); ?>
<?php $this->set('hideRelease', true); ?>

<ul class="breadcrumb">
    <li>O Município de Osasco</li>
</ul>

<p>
    <strong>Osasco</strong> é um município brasileiro localizado no estado deSão Paulo, na região metropolitana da
    capital paulista, e na microrregião a que dá nome. Antes um bairro da capital do estado, torna-se município
    emancipado em 19 de fevereiro de 1962. Tem como padroeiro Santo Antônio, sendo que no dia do santo 13 de junho é
    feriado municipal.
</p>

<h4>Período Pré-Colombiano</h4>
<p>
    Inúmeras tribos indígenas do tronco tupi-guarani habitavam a região desde o período pré-cabralino.
</p>

<h4>Período Colonial</h4>
<p>
    O primeiro núcleo de povoamento de colonizadores foi a vila de Quitaúna, fundada no século XVII, onde residiu o
    bandeirante Antônio Raposo Tavares e onde supostamente estaria enterrado. A vila de Quitaúna foi <em>esvaziada</em>
    no século XVIII com a descoberta do ouro em Minas Gerais.<br />
    Na região onde hoje se situa Osasco e em seus arredores existiam vários sítios e chácaras. Próximo às margens do
    Rio Tietê, no século XIX, havia uma aldeia depescadores e também grandes fazendas. Uma delas foi vendida ao italiano
    Antonio Agu, e outra ao português Manuel Rodrigues, dois imigrantes que começam a história da cidade.
</p>

<h4>Período Moderno</h4>
<p>
    Antonio Agù foi proprietário de vários negócios e terras na região e, em 1887, comprou uma gleba de terra no
    quilômetro 16 da Estrada de Ferro Sorocabana. Por volta de 1890, resolveu ampliar sua pequena olaria e convidou
    para sócio o Barão Sensaud de Lavaud. A olaria que fabricava tijolos e telhas passou a produzir também tubos e
    cerâmicas, dando origem à primeira indústria da cidade.<br />
    Após outras iniciativas, em 1895, Agu construiu a estação ferroviária, erguendo várias casas nos arredores para
    abrigar os operários que chegavam para realizar a obra.
</p>
<p>
    Os dirigentes da estrada de ferro quiseram batizar a estação com o nome do principal empreendedor da região, mas
    Antonio Agu pediu que a homenagem não fosse dada a ele e sim à sua vila natal da Itália: Osasco.
</p>
<p>
    Daí por diante Osasco, como a região passou a ser conhecida, não parava de crescer, muitas pessoas conhecidas do
    comércio e diversas indústrias importantes se instalaram por lá. Para operar as máquinas dessas indústrias foi
    contratada mão-de-obra imigrante.
</p>
<p>
    Os imigrantes vinham principalmente da Itália, França, Espanha, Portugal, Alemanha,Irlanda e também do Nordeste.
    Com o aumento da população de operários, tornou-se possível também o desenvolvimento do comércio, desenvolvido
    principalmente pelas colônias armênia ,libanesa e judia. Na zona rural, muitos imigrantes japonesesplantavam
    verduras e legumes. Essa mistura de imigrantes marca as primeiras populações do atual município.<br />
    Foi em Osasco que aconteceu o primeiro vôo da América Latina, pelo jovem entusiasta Barão Dimitri Sensaud de Lavand.
</p>

<h4>Emancipação</h4>
<p>
    Osasco cresceu tanto em população quanto comercialmente mas, apesar desse progresso, até então não passava de mero
    subdistrito da cidade de São Paulo. Em 1952 surgiram as primeiras manifestações pela emancipação. O movimento
    emancipacionista sofreu muitas contraposições e empecilhos, mas finalmente após um plebiscito conturbado, em 19 de
    fevereiro de 1962, Osasco tornou-se um município.
</p>
<p>
    No ano seguinte, o Banco Brasileiro de Descontos (atualmente denominado Bradesco), sediado na Cidade de Deus,
    bairro de Osasco próximo à divisa com São Paulo, organizou e colocou em operação a <em>Companhia Telefônica
        Suburbana Paulista - Cotespa</em>. A nova companhia inicialmente proveu o novo município de três mil terminais
    telefônicos, que operavam com o prefixo 48. A COTESPA foi incorporada à Telesp em 1974.
</p>
<p>
    A área do município de Osasco foi gradativamente subdividida em novos centros telefônicos -Rochdale, Santo Antônio,
    Quitaúna e Menck, além da área central.
</p>

<h4>Grandes acontecimentos históricos após a emancipação de Osasco</h4>
<ul>
    <li>
        <strong>A greve da Cobrasma</strong> foi em 16 de julho de 1968. Operários protestaram contra as mortes de seus
        colegas em caldeiras e o rebaixamento dos salários. Ato esse já um sintoma de resistência contra oRegime
        Militar da época.
    </li>
    <li>
        <strong>A explosão do</strong> Osasco Plaza Shopping em 11 de junho de 1996. O motivo foi vazamento de gás
        subterrâneo. Morreram 42 pessoas e 300 outras ficaram feridas, algumas gravemente. Esse acidente repercutiu
        na mídia nacional e internacional.
    </li>
</ul>

<h4>Administração pública</h4>
<p>Osasco teve os seguintes prefeitos:</p>
<ul>
    <li>Hirant Sanazar (19.02.1962 a junho de 1964; fevereiro a junho de 1965)</li>
    <li>Marino Pedro Nicoletti (junho de 1964 a fevereiro de 1965; interventor: 26.06.1965 a 31.01.1967)</li>
    <li>Guaçu Piteri (1967-1970)</li>
    <li>José Liberatti (1970-1973)</li>
    <li>Francisco Rossi (1º Mandato: 1973-1977)</li>
    <li>Guaçu Piteri (2º Mandato: 1977-1982)</li>
    <li>Primo Broseghini (1982-1983)</li>
    <li>Humberto Parro (1983-1988)</li>
    <li>Francisco Rossi (2º Mandato: 1989-1992)</li>
    <li>Celso Giglio (1º Mandato: 1993-1996)</li>
    <li>Silas Bortolosso (1997-2000)</li>
    <li>Celso Giglio (2º Mandato: 2001-2004)</li>
    <li>Emidio Pereira de Souza ( 2005 até 2008)</li>
    <li>Emidio Pereira de Souza (2º Mandato: 2009 até hoje)</li>
</ul>