<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Scaffolds
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="sidebar span4 pull-left">
    <? /*<h3><?php echo __d('cake', 'Actions'); ?></h3>*/ ?>

    <? echo $this->Html->link(
        $this->Html->image("web/logo.png", array("alt" => "2º Festival do Cachorro Quente de Osasco - SP", "title" => "2º Festival do Cachorro Quente de Osasco - SP", "id" => "lateral_logo")),
        "/",
        array('escape' => false)
    ) ?>

    <br>
    <br>

    <div class="row-fluid">
        <div class="well">
            <ul class="nav nav-list">
                <li><?php echo $this->Html->link(__d('cake', 'New %s', $singularHumanName), array('action' => 'add')); ?></li>
                <?php
                $done = array();
                foreach ($associations as $_type => $_data) {
                    foreach ($_data as $_alias => $_details) {
                        if ($_details['controller'] != $this->name && !in_array($_details['controller'], $done)) {
                            echo '<li>';
                            echo $this->Html->link(
                                __d('cake', 'List %s', Inflector::humanize($_details['controller'])),
                                array('plugin' => $_details['plugin'], 'controller' => $_details['controller'], 'action' => 'index')
                            );
                            echo '</li>';

                            echo '<li>';
                            echo $this->Html->link(
                                __d('cake', 'New %s', Inflector::humanize(Inflector::underscore($_alias))),
                                array('plugin' => $_details['plugin'], 'controller' => $_details['controller'], 'action' => 'add')
                            );
                            echo '</li>';
                            $done[] = $_details['controller'];
                        }
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<div class="main">
    <div class="<?php echo $pluralVar; ?> index">
        <h2><?php echo $pluralHumanName; ?></h2>
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <?php foreach ($scaffoldFields as $_field): ?>
                    <th><?php echo $this->Paginator->sort($_field); ?></th>
                <?php endforeach; ?>
                <th><?php echo __d('cake', 'Actions'); ?></th>
            </tr>
            <?php
            foreach (${$pluralVar} as ${$singularVar}):
                echo '<tr>';
                    foreach ($scaffoldFields as $_field) {
                        $isKey = false;
                        if (!empty($associations['belongsTo'])) {
                            foreach ($associations['belongsTo'] as $_alias => $_details) {
                                if ($_field === $_details['foreignKey']) {
                                    $isKey = true;
                                    echo '<td>' . $this->Html->link(${$singularVar}[$_alias][$_details['displayField']], array('controller' => $_details['controller'], 'action' => 'view', ${$singularVar}[$_alias][$_details['primaryKey']])) . '</td>';
                                    break;
                                }
                            }
                        }
                        if ($isKey !== true) {
                            echo '<td>' . h(${$singularVar}[$modelClass][$_field]) . '</td>';
                        }
                    }

                    echo '<td>';
                        echo '<div class="btn-group">';
                            echo $this->Html->link('<i class="icon-plus"></i>', array('action' => 'view', ${$singularVar}[$modelClass][$primaryKey]), array('class' => 'btn btn-small', 'escape' => false));
                            echo ' ' . $this->Html->link('<i class="icon-pencil"></i>', array('action' => 'edit', ${$singularVar}[$modelClass][$primaryKey]), array('class' => 'btn btn-small', 'escape' => false));
                            echo ' ' . $this->Form->postLink(
                                '<i class="icon-trash"></i>',
                                array('action' => 'delete', ${$singularVar}[$modelClass][$primaryKey]),
                                array('class' => 'btn btn-small', 'escape' => false),
                                __d('cake', 'Are you sure you want to delete').' #' . ${$singularVar}[$modelClass][$primaryKey]
                            );
                        echo '</div>';
                    echo '</td>';
                echo '</tr>';
            endforeach;
            ?>
        </table>
        <div class="row-fluid">
            <div class="span6">
                <div class="pagination">
                    <ul>
                        <?php
                        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled', 'disabledTag' => 'a'));
                    ?>
                    </ul>
                </div>
            </div>
            <div class="span6">
                <?php
                    echo $this->Paginator->counter(array(
                        'format' => __d('cake', 'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    ));
                    ?>
            </div>
        </div>
    </div>
</div>