<? $this->set("title_for_layout", "Importação de votos"); ?>

<?php $this->set('hideBanner', true); ?>
<?php $this->set('hideRelease', true); ?>

<?php echo $this->Html->script('custom/importar-votos.js', array('inline' => false)); ?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao">Votos</a> <span class="divider">&raquo;</span></li>
    <li>Importação</li>
</ul>

<div id="alerta"></div>
<div id="arquivos"></div>
<h1>Restam <span id="validos"></span></h1>
<div class="row-fluid">
<?php echo
    $this->Form->create('Importacao', array(
        'url' => 'javascript:submit();',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'control-group'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
        ))
    );
?>
    <div class="row-fluid">
        <div class="span8">
            <input type="file" multiple="multiple" />
        </div>
        <div class="span4">
            <input class="btn btn-primary" type="submit" value="Enviar" />
        </div>
    </div>
<?php
    echo $this->Form->end();
?>
</div>