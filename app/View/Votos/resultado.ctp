<? $this->set("title_for_layout", "Resultado da votação"); ?>

<?php $this->set('hideBanner', true); ?>
<?php $this->set('hideRelease', true); ?>

<?php echo $this->Html->script('custom/resultado.js', array('inline' => false)); ?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao">Votação</a> <span class="divider">&raquo;</span></li>
    <li>Resultado</li>
</ul>

<h1 class="text-center" style="color:#bbb; font-weight:normal;"><span id="total" style="color:#CC0000; font-weight:bolder;">0</span> votos</h1>

<table id="resultado" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Posição</th>
            <th>Número</th>
            <th>Nome</th>
            <th>Votos</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <? foreach ($votos as $voto_index => $voto): ?>
        <tr>
            <td><?php echo ++$voto_index ?>º</td>
            <td><?php echo $voto['Dogueiro']['numero'] ?></td>
            <td><?php echo $voto['Dogueiro']['nome'] ?></td>
            <td><?php echo $voto['0']['votos'] ?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>