<? $this->set("title_for_layout", "Votação"); ?>

<?php $this->set('hideRelease', true); ?>

<?
    $this->Html->css('web/form', null, array('inline' => false));
    $this->Html->script('custom/voto', array('inline' => false));
    echo $this->Session->flash();
?>

<ul class="breadcrumb">
    <li>Qual seu dogueiro preferido em Osasco?</li>
</ul>

<p>
    Os <strong>20</strong> finalistas participarão do 2º Festival do Cachorro Quente de Osasco e concorrerão a prêmios de milhares de reais.
</p>

<blockquote>Não sabe o número do dogueiro? <a href="dogueiros/galeria" target="_top">Clique aqui</a> e conheça os inscritos.</blockquote>

<div class="row-fluid">
    <?
    echo $this->Session->flash();
    echo $this->Form->create('Voto');
    echo $this->Form->input('email', array('label' => 'Seu e-mail (precisa ser válido)', 'class' => 'span12'));
    echo $this->Form->input('dogueiro_id', array('type' => 'hidden'));
    echo $this->Form->input('numero', array('label' => 'Nº de inscrição do dogueiro (encontrado em seu carrinho)', 'class' => 'span12', 'maxlength' => '3', 'autocomplete' => 'off'));
    ?>
    <div class="row-fluid text-center">
        <input type="button" class="btn btn-large key" value="1">
        <input type="button" class="btn btn-large key" value="2">
        <input type="button" class="btn btn-large key" value="3">
        <input type="button" class="btn btn-large key" value="4">
        <input type="button" class="btn btn-large key" value="5">
        <input type="button" class="btn btn-large key" value="6">
        <input type="button" class="btn btn-large key" value="7">
        <input type="button" class="btn btn-large key" value="8">
        <input type="button" class="btn btn-large key" value="9">
        <input type="button" class="btn btn-large key" value="0">
    </div>
    <div class="row-fluid">
        <div class="text-center">
            <h3 id="dogueiro-nome"></h3>
            <img id="dogueiro-foto" src="" alt="" class="hide">
        </div>
    </div>
    <hr>
    <div class="row-fluid">
    <?
    echo $this->Form->button('Limpar', array('type' => 'reset', 'class' => 'btn btn-danger pull-left'));
    echo $this->Form->button('Votar', array('type' => 'submit', 'class' => 'btn btn-primary pull-right'));
    echo $this->Form->end();
    ?>
    </div>
    <small>*Você pode votar apenas <span class="text-error">uma vez por dia</span>. <?php #Qualquer outro voto no mesmo dia será invalidado. ?></small>
</div>

<p class="dev">
    Sistema de votação desenvolvido por: <a href="http://www.linkedin.com/in/regissilva" target="_blank">Regis Silva</a>
</p>