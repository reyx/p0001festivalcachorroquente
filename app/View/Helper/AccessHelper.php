<?
/**
 * Access helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Access
 */
class AccessHelper extends Helper {
    var $helpers = array("Session");

    function isLoggedin() {
        App::import('Component', 'Auth');
        $auth = new AuthComponent();
        $auth->Session = $this->Session;
        $user = $auth->user();
        return !empty($user);
    }
}
