<? $this->set("title_for_layout", "Localização"); ?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao/">Sobre o Festival</a> <span class="divider">&raquo;</span></li>
    <li>Localização</li>
</ul>

<p>A <strong>grande final</strong> acontecerá  no <strong>Bicicletário - em frente à estação Osasco.</strong></p>
<p class="label label-important">25 de Maio de 2013 | 09:00 as 17:00</p>

<?php echo $this->Html->image('festival_osasco_bicicletario.jpg', array('alt' => 'Bicicletário Osasco', 'title' => 'Bicicletário Osasco')); ?>

<p class="more"><? echo $this->Html->link('Saiba mais...', array('controller' => 'sobre', 'action' => 'equipe')) ?></p>
