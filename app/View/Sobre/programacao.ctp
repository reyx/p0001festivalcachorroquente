<? $this->set("title_for_layout", "Programação"); ?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao/">Sobre o Festival</a> <span class="divider">&raquo;</span></li>
    <li>Programação</li>
</ul>

<? foreach ($atividades as $index => $atividade): ?>
    <p><? echo date("H:i", strtotime($atividade['Atividade']['hora'])) . ' - ' . $atividade['Atividade']['evento'] ?></p>
<? endforeach; ?>

<p class="more"><? echo $this->Html->link('Saiba mais...', array('controller' => 'sobre', 'action' => 'premiacoes')) ?></p>

<br>

<blockquote>Além das atrações o evento terá cobertura de emissoras de <strong>televisão</strong>,
    <strong>mídia impressa</strong> e <strong>emissoras de rádio</strong>.
</blockquote>

<blockquote>Atrações e horários sujeitos à alterações.</blockquote>
