<? $this->set("title_for_layout", "Premiações"); ?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao/">Sobre o Festival</a> <span class="divider">&raquo;</span></li>
    <li>Premiações</li>
</ul>

<p>
    <strong>
        <span class="label label-important">Prêmio principal</span> <span class="underline">O melhor Cachorro Quente de Osasco</span>
    </strong>
</p>
<ul>
    <li><strong>1º</strong> colocado: prêmio de <strong>R$ 5.000,00</strong> (Cinco mil reais) + Troféu;</li>
    <li><strong>2º</strong> colocado: prêmio de <strong>R$ 3.000,00</strong> (Três mil reais) + Troféu;</li>
    <li><strong>3º</strong> colocado: prêmio de <strong>R$ 2.000,00</strong> (Dois mil reais) + Troféu.</li>
    <li><strong>4º</strong> ao <strong>30º</strong> colocado: Medalha de participação.</li>
</ul>
<blockquote>A inscrição do dogueiro será <strong>gratuita</strong>, e realizada pela internet.<br>
    O vencedor manterá o titulo de <strong>&quot;Melhor Cachorro Quente de Osasco&quot;</strong> pelo  período de
    <strong>1 (um) ano</strong>.
</blockquote>
<p>
    <strong>
        <span class="label label-important">Prêmio extra</span> <span class="underline">O maior comedor de Cachorros Quentes de Osasco</span>
    </strong>
</p>
<ul>
    <li><strong>1º</strong> colocado: prêmio de <strong>R$ 500,00</strong> (Quinhentos reais) + Troféu.</li>
</ul>
<blockquote>A inscrição será <strong>gratuita</strong> e aberta ao público, no evento.<br />
    Serão sorteados <strong>15 participantes</strong>, pouco antes do início da competição.
</blockquote>

<p class="more"><? echo $this->Html->link('Saiba mais...', array('controller' => 'sobre', 'action' => 'localizacao')) ?></p>
