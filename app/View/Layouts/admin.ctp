<?
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', '2º Festival do Cachorro Quente de Osasco');
?>
<!DOCTYPE html>
<html>
<head>
    <? echo $this->Html->charset(); ?>
    <title>
        <? echo $cakeDescription ?>:
        <? echo $title_for_layout; ?>
    </title>
    <?
        echo $this->Html->meta('icon');

        // echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        echo $this->Less->css('bootstrap/bootstrap');
        echo $this->Less->css('web/admin');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <? echo $this->Html->link($cakeDescription, 'http://www.festivaldocachorroquente.com.br', array('class' => 'brand')); ?>
                <div class="nav-collapse collapse">
                    <p class="navbar-text pull-right">
                        Logged in as <a href="#" class="navbar-link">Username</a>
                    </p>
                    <ul class="nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <? echo $this->Session->flash(); ?>
        <? echo $this->fetch('content'); ?>
    </div>

    <footer>
        <? echo $this->Html->link(
            'ReyX',
            'http://www.reyx.com.br/',
            array('target' => '_blank', 'escape' => false));
        ?>
    </footer>
</body>
</html>
