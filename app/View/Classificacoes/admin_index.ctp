<table>
    <tr>
        <th>Dogueiro</th>
        <th>Posicao</th>
        <th>Ano</th>
        <th></th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($classificacoes as $classificacao): ?>
        <tr>
            <td><?php echo $classificacao['Dogueiro']['nome']; ?></td>
            <td><?php echo $classificacao['Classificacao']['posicao']; ?></td>
            <td><?php echo $classificacao['Classificacao']['ano']; ?></td>
            <td>
                <?php echo $this->Html->link('Editar',
                    array('controller' => 'classificacoes', 'action' => 'admin_edit', $classificacao['Classificacao']['id'])); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php unset($post); ?>
</table>