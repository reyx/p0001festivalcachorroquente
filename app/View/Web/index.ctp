<? $this->set("title_for_layout", "Repercussão 2012"); ?>

<? $this->Html->css('web/base', null, array('inline' => false)); ?>

<p class="align-center">
    <? echo $this->Html->image('web/festival_mapa_festival_osasco.png', array('title' => '25 de Maio de 2013 - Estação Osasco')) ?>
</p>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao">Sobre o Festival</a> <span class="divider">&raquo;</span></li>
    <li>Repercussão em 2012</li>
</ul>

<?php echo $this->element('release'); ?>

<p class="align-center">
    <object width="560" height="315">
        <param name="movie" value="http://www.youtube.com/v/Inek0d0ZhqM?version=3&amp;hl=pt_BR">
        <param name="allowFullScreen" value="true">
        <param name="allowscriptaccess" value="always">
        <param name="wmode" value="transparent">
        <embed src="http://www.youtube.com/v/Inek0d0ZhqM?version=3&amp;hl=pt_BR" width="560" type="application/x-shockwave-flash" height="315" allowscriptaccess="always" allowfullscreen="true" movie="http://www.youtube.com/v/Inek0d0ZhqM?version=3&amp;hl=pt_BR" wmode="transparent">
    </object>
</p>

<p>
    A fama do cachorro quente de Osasco cruzou o Oceano Atlântico, chegando
    até a Itália, de onde recebemos a visita de “Stefano Marras” <em>(Ph.D.,
    Research Fellow Department of Sociology and Social Research – Università
    degli Studi di Milano-Bicocca)</em> que vem percorrendo o mundo com um
    documentário sobre comidas de rua que será apresentado em um livro e um
    vídeo, por ocasião da <strong> <em>Expo Milão 2015</em></strong>, sobre
    o tema <em>"Alimentação do planeta. Energia para a vida“</em>.
</p>

<p>
    Em fevereiro de 2012, as principais emissoras de TV do país assestaram suas objetivas para a realização, proclamando Osasco como <em> <strong>“A segunda cidade que mais consome cachorro-quente no mundo, atrás apenas de Nova Iorque”</strong> </em> – <em>Revista Viver Osasco</em>
</p>

<p>Agradecemos a todas as emissoras que apoiaram o projeto e nomearam Osasco como a <strong>“Capital do Cachorro Quente”</strong>...</p>

<p class="align-center">
    <? echo $this->Html->image('web/logos_emissoras.jpg', array('title' => 'Globo, Record, Band, SBT, Rede TV!, TV Osasco, NGT')) ?>
</p>

<p>
    O evento ocupou espaço nas principais emissoras do país, em programas de
    grande audiência e em horários nobres, sendo alguns deles: SPTV, Claquete
    (Otavio Mesquita), Dia a Dia, Jornal do SBT, Programa Manhã Maior,
    Programa Hoje em Dia, Programa Amaury Jr... dentre outros.
</p>

<p>
    No total, foram mais de <strong>3 horas no ar</strong>, nas principais
    emissoras do Brasil, sem contar a <strong>entrevista de 25 minutos</strong>
    concedida à TV Osasco, que foi reprisada 8 vezes a pedido de telespectadores!
</p>

<br>

<p>
    <strong>Confira alguns destes momentos...</strong>
</p>

<? foreach ($destaques as $index => $materia): ?>
    <? if ($index == 0): ?>
    <div class="row-fluid">
    <? endif ?>
        <div class="span6">
            <div class="materia">
                <strong><? echo $materia['Materia']['title'] ?> <span class="label"><? echo $materia['Materia']['label'] ?></span></strong>
                <br>
                <? if ($materia['Materia']['type'] == 0): ?>
                <a href="<? echo $materia['Materia']['link'] ?>" target="_blank">
                    <img src="<? echo $this->webroot . 'files/materia/img/' . $materia['Materia']['img_dir'] . '/' . $materia['Materia']['img'] ?>" alt="<? echo $materia['Materia']['program'] ?>" title="<? echo $materia['Materia']['program'] ?>">
                </a>
                <? elseif ($materia['Materia']['type'] == 1): ?>
                <object width="250" height="199">
                    <param name="movie" value="<? echo $materia['Materia']['link'] ?>">
                    <param name="allowFullScreen" value="true">
                    <param name="allowscriptaccess" value="always">
                    <param name="wmode" value="transparent">
                    <embed src="<? echo $materia['Materia']['link'] ?>" type="application/x-shockwave-flash" width="250" height="199" allowscriptaccess="always" allowfullscreen="true" wmode="transparent">
                </object>
                <? endif; ?>
            </div>
        </div>
    <? if ($index & 1 == 1): ?>
    </div>
    <div class="row-fluid">
    <? endif ?>
    <? if ($index == sizeof($destaques) - 1): ?>
    </div>
    <? endif ?>
<? endforeach; ?>

<br>
<br>

<h4>VEJA TAMBÉM</h4>

<br>

<? foreach ($chamadas as $materia): ?>
<div class="media">
    <a class="pull-left" href="<? echo $materia['Materia']['link'] ?>">
        <img class="media-object" src="<? echo $this->webroot . 'files/materia/img/' . $materia['Materia']['img_dir'] . '/' . $materia['Materia']['img'] ?>" title="<? echo $materia['Materia']['label'] ?>">
    </a>
    <div class="media-body">
        <strong class="media-heading"><? echo $materia['Materia']['title'] ?> <span class="label"><? echo $materia['Materia']['label'] ?></span></strong>
        <br>
        <a href="<? echo $materia['Materia']['link'] ?>" target="_blank">Leia aqui...</a>
    </div>
</div>
<br>
<? endforeach; ?>

<p class="more"><? echo $this->Html->link('Saiba mais...', array('controller' => 'makingofs')) ?></p>
