<? $this->set("title_for_layout", "Obrigado por se inscrever"); ?>

<h4>
    Obrigado, <strong><? echo $dogueiro['Dogueiro']['nome'] ?></strong>. Será um prazer tê-lo conosco!
</h4>

<p>
    Seu cadastro foi enviado.
    Suas informações serão validadas e você receberá um e-mail, confirmando seu número de inscrição.
</p>
