<? $this->set("title_for_layout", "Galeria"); ?>

<?php $this->set('hideRelease', true); ?>

<?
    $this->Html->css('fancybox/jquery.fancybox.css', null, array('inline' => false));
    $this->Html->css('fancybox/jquery.fancybox-buttons.css', null, array('inline' => false));

    $this->Html->script('fancybox/jquery.mousewheel-3.0.6.pack.js', array('inline' => false));
    $this->Html->script('fancybox/jquery.fancybox.pack.js', array('inline' => false));
    $this->Html->script('fancybox/jquery.fancybox-buttons.js', array('inline' => false));
    $this->Html->script('jquery.imagesloaded.min.js', array('inline' => false));
    $this->Html->script('custom/bastidores.js', array('inline' => false));
?>

<ul class="breadcrumb">
    <li><a href="/sobre/introducao/">Dogueiros</a> <span class="divider">&raquo;</span></li>
    <li>Galeria</li>
</ul>

<blockquote>
    * Em algumas fotos as pessoas são funcionárias. No entanto, o cadastro está feito em nome do dogueiro licenciado.
</blockquote>

<div class="row-fluid makingof fade">
<? foreach ($dogueiros as $dogueiro_index => $dogueiro) {
    if ($dogueiro['Dogueiro']['facebook_foto_dir']) {
        echo $this->Html->link(
            $this->Html->image('/files/dogueiro/facebook_foto/' . $dogueiro['Dogueiro']['facebook_foto_dir'] . '/thumb_' . $dogueiro['Dogueiro']['facebook_foto'], array('alt' => '')),
            '/files/dogueiro/facebook_foto/' . $dogueiro['Dogueiro']['facebook_foto_dir'] . '/' . $dogueiro['Dogueiro']['facebook_foto'],
            array(
                'class' => 'fancybox',
                'rel' => 'gallery',
                'title' => 'Galeria de Dogueiros',
                'escape' => false
            )
        );
    }
} ?>