<? $this->set("title_for_layout", "Vencedores de 2012"); ?>

<?php $this->set('hideRelease', true); ?>

<ul class="breadcrumb">
    <li>Os 30 Melhores de Osasco em 2012</li>
</ul>

<? if ($list): ?>
<ul class="nav top30">
<? foreach ($list as $index => $dogueiro): ?>
    <? if ($dogueiro['Classificacao']['posicao'] <= 3): ?>
        <li><strong class="top3"><? echo $dogueiro['Dogueiro']['nome'] ?></strong>
            <? if ($dogueiro['Classificacao']['posicao'] == 1): ?>
                <? echo $this->Html->image('1st.png') ?>
            <? elseif ($dogueiro['Classificacao']['posicao'] == 2): ?>
                <? echo $this->Html->image('2nd.png') ?>
            <? else: ?>
                <? echo $this->Html->image('3rd.png') ?>
            <? endif; ?>
    <? elseif ($dogueiro['Classificacao']['posicao'] <= 10): ?>
        <li><strong><? echo $dogueiro['Dogueiro']['nome'] ?></strong> <? echo $this->Html->image('top10.png') ?></li>
    <? else: ?>
        <li><? echo $dogueiro['Dogueiro']['nome'] ?></li>
    <? endif; ?>
<? endforeach; ?>
</ul>
<? endif; ?>

<p class="label label-warning">* Os dogueiros estão listados em ordem alfabética.</p>
